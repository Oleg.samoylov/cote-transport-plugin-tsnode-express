import {IPlugin, ConfigProvider, Application} from "tsnode-express";
import {success, warning, error, info} from 'nodejs-lite-logger';
import { Responder, Requester} from 'cote';
import { matchPattern, getParams } from 'url-matcher';
import { IMicroHandler, IMicroHandlerOptions } from "./interfaces";
import { PLUGIN_NAME } from "./index";

@Reflect.metadata('design', 'paramtypes')
export class CoteTransportProvider implements IPlugin {
    microHandlers: Map<string, IMicroHandler[]> = new Map<string, IMicroHandler[]>();
    requesters: Map<string, Requester> = new Map<string, Requester>();

    constructor(public application: Application, private config: ConfigProvider) {
        (this.application.Injector.getPlugin(PLUGIN_NAME).handlers || []).forEach((handler)=> {
           let h = this.microHandlers.get(handler.method);
           h ? h.push(handler) : this.microHandlers.set(handler.method, [handler]);
        });
        this.microHandlers.forEach((value, key) => this.microHandlers.set(key, value.sort(this.sortByParams('url'))));
        const responder = new Responder({
            name: `RESPONDER for ${config.responderKey}`,
            namespace: config.responderKey
        });
        responder.on(config.responderKey, this.requestHandler.bind(this));

    }

    request(options: IMicroHandlerOptions): Promise<any> {
        let requester = this.requesters.get(options.type);
        if (!requester) {
            requester = new Requester({name: 'REQUESTER', namespace: options.type, timeout: 120000});
            this.requesters.set(options.type, requester);
        }
        return requester.send(options);
    }

    private async requestHandler(req, cb: Function) {
        info(req.method, req.url);
        const matchedRoute = (this.microHandlers.get(req.method) || []).find(({url}) => {
            const params = {...getParams(url, req.url)};
            Object.keys(params).length && Object.assign(req.payload, { params });
            return !!matchPattern(url, req.url);
        });

        if (matchedRoute) {
            const controller = this.application.Injector.resolve(matchedRoute.controllerName);
            try {
                const result = await matchedRoute.handler.call(controller, req.payload);
                success(result);
                return result;
            } catch (e) {
                error(e)
                return {status: e.status || 500, message: e.message || e};
            }
        } else {
            error('Handler not found');
            return {status: 404, message: 'Handler not found'};
        }
    }

    private sortByParams(urlParam: string) {
        return (first: any, second: any) => {
            const fSplittedUrl = first[urlParam].split('/');
            const sSplittedUrl = second[urlParam].split('/');
            const fParamIndex = fSplittedUrl.findIndex((i) => i.startsWith(':'));
            const sParamIndex = sSplittedUrl.findIndex((i) => i.startsWith(':'));
            if ((fParamIndex > sParamIndex || fParamIndex === -1) || fSplittedUrl.length > sSplittedUrl.length) {
                return -1;
            } else if ((fParamIndex < sParamIndex || sParamIndex === -1) || fSplittedUrl.length > sSplittedUrl.length) {
                return 1;
            }
            return 0;
        }
    }
}



