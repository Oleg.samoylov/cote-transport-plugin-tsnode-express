import {MicroController, MicroHandler} from "../index";
import {CoteTransportProvider} from "../index";
import {HttpMethods, IRequestArguments} from "tsnode-express";


@MicroController()
export class TestController {

    constructor(private transport: CoteTransportProvider){}

    @MicroHandler({ url: 'test', method: HttpMethods.GET })
    getSomeData(){
        return { data: [{name: 1}, {name: 2}] };
    }

    @MicroHandler({ url: 'test/:id', method: HttpMethods.GET })
    getSingleData(payload: IRequestArguments){
        return { data: 'single-data', id: payload.params.id };
    }

    @MicroHandler({ url: 'test/test', method: HttpMethods.GET })
    getData(){
        return { data: "some-data" };
    }

    @MicroHandler({ url: 'echo', method: HttpMethods.POST })
    postSingleData(req: IRequestArguments){
        return req;
    }

    @MicroHandler({ url: 'data-from-other-service', method: HttpMethods.POST })
    testTransport(payload: IRequestArguments){
        const data = {
            payload,
            url: 'remote',
            type: 'other-microservice',
            method: HttpMethods.GET
        };
        return this.transport.request(data);
    }

}