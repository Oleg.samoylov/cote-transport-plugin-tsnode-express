import { Application } from "tsnode-express";
import { CoteTransportProvider } from "../index";
import { TestController } from "./controller";
const application = new Application();

function setConfig(config){
    config.responderKey = 'test';
}

application
    .usePlugin(CoteTransportProvider)
    .useConfig(setConfig)
    .registerModule(TestController);

export default application;