import test from 'tape';
import { Requester, Responder } from 'cote';
import application from "./application";
import {HttpMethods} from "tsnode-express";
const requester = new Requester({ name: 'REQUESTER', namespace: 'test', timeout: 120000  });

test('start server', async (t) => {
    await application
        .start(() => {
            console.log('Server started');
            t.end()
        });
});

test('Request to `test` with GET method', async (t) => {
    const requestData = { type: 'test', url: 'test', method: HttpMethods.GET , payload: { testField: 'test'}};
    const result = await requester.send(requestData);
    t.deepEqual(result,{ data: [ { name: 1 }, { name: 2 } ] });
    t.end();
});

test('Request to `test/:id` with GET method', async (t) => {
    const id = '111';
    const requestData = { type: 'test', url: `test/${id}`, method: HttpMethods.GET , payload: { testField: 'test'}};
    const result = await requester.send(requestData);
    t.deepEqual(result,{ data: 'single-data', id });
    t.end();
});

test('Request to similar route `test/test` with GET method', async (t) => {
    const requestData = { type: 'test', url: `test/test`, method: HttpMethods.GET , payload: {}};
    const result = await requester.send(requestData);
    t.deepEqual(result,{ data: 'some-data'});
    t.end();
});

test('Request to `echo` with method `POST` should return same payload', async (t) => {
    const payload = { test1: 'test1', test2: 'test2'};
    const requestData = { type: 'test', url: 'echo', method: HttpMethods.POST , payload };
    const result = await requester.send(requestData);
    t.deepEqual(result, payload);
    t.end();
});

test('Test request method should take data from other microservice', async (t) => {
    const responder = new Responder({ name: 'RESPONDER', namespace: 'other-microservice'});
    responder.on('other-microservice', (req, cb) => {
        cb(null, {...req.payload, custom: 'custom-value'});
    });
    const payload = { testField: 'test' };
    const requestData = { type: 'test', url: 'data-from-other-service', method: HttpMethods.POST , payload };
    const result = await requester.send(requestData);
    t.deepEqual(result, {...payload, custom: 'custom-value'});
    t.end();
});


test('exit tests', (t) => {
    t.end();
    process.exit(0);
})
