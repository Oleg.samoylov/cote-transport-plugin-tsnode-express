import { HttpMethods, IRequestArguments } from "tsnode-express";

export interface IMicroHandlerOptions {
    type: string;
    url: string;
    method: HttpMethods;
    payload?: IRequestArguments,
}

export interface IMicroHandler extends IMicroHandlerOptions {
    controllerName: string,
    handler: Function,
    fname: string
}
