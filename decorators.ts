import {IMicroHandler, IMicroHandlerOptions} from "./interfaces";
import { PLUGIN_NAME } from "./index";
import { Injector } from "tsnode-express";

function MicroHandlerDecorator(opts: IMicroHandlerOptions): Function {
    return (target: any, fname: string, descriptor: PropertyDescriptor): void => {
        const obj: IMicroHandler = {
            fname,
            handler: descriptor.value, ...opts,
            controllerName: target.constructor.name
        };

        const plugin: any = this.plugins.get(PLUGIN_NAME);

        if(!plugin) {
            this.plugins.set(
                PLUGIN_NAME,
                {
                    handlers: [obj],
                    injections: []
                }
            )
        } else {
            plugin.handlers.push(obj)
        }
    }
}

function MicroControllerDecorator(){
    return (target) : void => {
        this.set(target);
        const plugin: any = this.plugins.get(PLUGIN_NAME);

        if(!plugin) {
            this.plugins.set(
                PLUGIN_NAME,
                {
                    injections: [target.name],
                    handlers: []
                }
            )
        } else {
            plugin.injections.push(target.name)
        }
    }
}

export const MicroHandler: Function = MicroHandlerDecorator.bind(Injector.getInstance());
export const MicroController: Function = MicroControllerDecorator.bind(Injector.getInstance());